<?php
// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');

require_once('dibsClass.php');

/**
 * Adds Dibs_Widget widget.
 */
class Dibs_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Dibs_Widget', // Base ID
			__('Dibs Ads Widget', 'Dibs Ads Widget'), // Name
			array( 'description' => __( 'Dibs Ads Widget', 'dibs.my' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
	
	    // Get user detail
	    $current_user = wp_get_current_user();
	    $currentAdsType = $instance['dibs_ads_target_layout'];

	    if($instance['dibs_ads_target_layout'] == "3") {
	    	$currentAdsType = "dibs-ads3";
	    }
	    if($instance['dibs_ads_target_layout'] == "4") {
	    	$currentAdsType = "dibs-ads4";
	    }
	    if($instance['dibs_ads_target_layout'] == "16") {
	    	$currentAdsType = "dibs-ads16";
	    }
	
     	echo $args['before_widget'];
        ?>

        <div class="<?php echo $currentAdsType; ?>">
            <input type="hidden" name="dibs-user" value="<?php echo $current_user->user_email; ?>">
            <input type="hidden" name="dibs-ads-type" value="<?php echo $instance['dibs_ads_target_layout']; ?>">
            <div class="dibs-ads-container">
    		</div>
		</div>
		<?php
		
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {		
		$dibs_ads_target_layout = null;
		if ( isset( $instance[ 'dibs_ads_target_layout' ] ) ) {
			$dibs_ads_target_layout = $instance[ 'dibs_ads_target_layout' ];
		}
		
		?>
		<p>
            <label><?php _e( 'Layout Type:' ); ?></label>
            <select name="<?php echo $this->get_field_name( 'dibs_ads_target_layout' ); ?>" data-placeholder="Layout type">
                <option <?php if ($dibs_ads_target_layout=='4') {_e('selected');} ?> value="4">4 x 3 ratio</option>
                <option <?php if ($dibs_ads_target_layout=='16') {_e('selected');} ?> value="16">16 x 9 ratio</option>
                <option <?php if ($dibs_ads_target_layout=='3') {_e('selected');} ?> value="3">3 x 6 ratio</option>
            </select>
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['dibs_ads_target_layout'] = ( ! empty( $new_instance['dibs_ads_target_layout'] ) ) ? strip_tags( $new_instance['dibs_ads_target_layout'] ) : '';

		return $instance;
	}

} // class Dibs_Widget