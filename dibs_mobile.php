<?php

if (isset($_POST['tag']) && $_POST['tag']!='') {
	$tag = $_POST['tag'];
	$api_key = "08330425af";
	$response = array("tag" => $tag, "success" => 0, "error" => 0);
	$dibsfunction = new DIBSFunction();

	if ($tag == 'get_ads_request') {
		$user_email = (isset($_POST['user_email']) && $_POST['user_email']!='') ? $_POST['user_email'] : '';
		$ip_address = (isset($_POST['ip_address']) && $_POST['ip_address']!='') ? $_POST['ip_address'] : '';
		$width_ratio = (isset($_POST['width_ratio']) && $_POST['width_ratio']!='') ? $_POST['width_ratio'] : '';
		$ads_count = (isset($_POST['ads_count']) && $_POST['ads_count']!='') ? $_POST['ads_count'] : '';
		$latitude = (isset($_POST['latitude']) && $_POST['latitude']!='') ? $_POST['latitude'] : '';
		$longtitude = (isset($_POST['longtitude']) && $_POST['longtitude']!='') ? $_POST['longtitude'] : '';

		$result = $dibsfunction->adsRequest($api_key, $user_email, $latitude, $longtitude, $ip_address, $width_ratio, $ads_count);

		if ($result != false) {
			echo json_encode($result);
		} else {

			$response["error"] = 1;
			$response["error_msg"] = "No ads display";

			echo json_encode($response);

		}

	} elseif ($tag == 'trigger_action') {

		$user_email = (isset($_POST['user_email']) && $_POST['user_email']!='') ? $_POST['user_email'] : '';
		$unique_key = (isset($_POST['unique_key']) && $_POST['unique_key']!='') ? $_POST['unique_key'] : '';
		$trigger_tag = (isset($_POST['trigger_tag']) && $_POST['trigger_tag']!='') ? $_POST['trigger_tag'] : '';

        // Check tag, only set action name
		if($trigger_tag == 'comment') {
			$action_name = 'comment';
		} else if($trigger_tag == 'vote_mood') {
			$action_name = 'vote_mood';
		} else if($trigger_tag == 'daily_login') {
			$action_name = 'daily_login';
		} else if($trigger_tag == 'share') {
			$action_name = 'share';
		} else if($trigger_tag == 'view_link') {
			$action_name = 'view_link';
			$unique_key	= null;
		} else if($trigger_tag == 'comment_like') {
			$action_name = 'comment_like';
		}

		$result = $dibsfunction->actionRequest($api_key, $user_email, $unique_key, $action_name);

		if ($result != false) {
			echo json_encode($result);
		} else {
			$response['IS_SUCCESS'] = 0;
			$response["error"] = 1;
			$response["error_msg"] = "No action trigger";

			echo json_encode($response);

		}
	} elseif ($tag == 'trigger_cpc') {
		$user_email = (isset($_POST['user_email']) && $_POST['user_email']!='') ? $_POST['user_email'] : '';
		$ads_id = (isset($_POST['ads_id']) && $_POST['ads_id']!='') ? $_POST['ads_id'] : '';
		$action_id = (isset($_POST['action_id']) && $_POST['action_id']!='') ? $_POST['action_id'] : '';
		$latitude = (isset($_POST['latitude']) && $_POST['latitude']!='') ? $_POST['latitude'] : '';
		$longtitude = (isset($_POST['longtitude']) && $_POST['longtitude']!='') ? $_POST['longtitude'] : '';

		$result = $dibsfunction->triggerCpcRequest($api_key, $user_email, $ads_id, $action_id, $latitude, $longtitude);

		if ($result != false) {
			echo json_encode($result);
		} else {
			$response['IS_SUCCESS'] = 0;
			$response["error"] = 1;
			$response["error_msg"] = "Trigger cpc failed";

			echo json_encode($response);

		}

	} elseif ($tag == 'get_dibs_profile') {
		$user_email = (isset($_POST['user_email']) && $_POST['user_email']!='') ? $_POST['user_email'] : '';

		$result = $dibsfunction->dibsProfileRequest($api_key, $user_email);

		if ($result != false) {
			echo json_encode($result);
		} else {
			$response['IS_SUCCESS'] = 0;
			$response["error"] = 1;
			$response["error_msg"] = "Get user profile failed";

			echo json_encode($response);

		}
	} elseif($tag == 'get_user_point') {
		$user_email = (isset($_POST['user_email']) && $_POST['user_email']!='') ? $_POST['user_email'] : '';

		$result = $dibsfunction->dibsPointRequest($api_key, $user_email);

		if ($result != false) {
			echo json_encode($result);
		} else {
			$response['IS_SUCCESS'] = 0;
			$response["error"] = 1;
			$response["error_msg"] = "Get user profile failed";

			echo json_encode($response);

		}
	} elseif($tag == 'getminimumsupportedversion') {
		$response ["IS_SUCCESS"] = 1;
		$response ["force_update"] = 0; // 1 is force update.
		$response ["minimumSupportedVersion"] = 8;
		echo json_encode ( $response );
	} elseif($tag == 'login_dibs_store') {
		$user_email = (isset($_POST['user_email']) && $_POST['user_email']!='') ? $_POST['user_email'] : '';

		$result = $dibsfunction->dibsStoreLoginRequest($user_email);

		if ($result != false) {
			$response["url"] = $result;
			echo json_encode($response);
		} else {
			$response['IS_SUCCESS'] = 0;
			$response["error"] = 1;
			$response["error_msg"] = "Failed to auto login.";

			echo json_encode($response);
		}

	}else {


		echo "Invalid Request";

	}
} else {

	echo "Access Denied";
}

class DIBSFunction{

		// Dibs login url
	public $dibsLoginUrl = 'http://dibs.my/store/login_store/';
	// Kwong wah Hash
	public $hash = 'WafpPsct9hNLdnw5XPU1GxPoFEZjYzU0ZGYxZTk2';


	public function adsRequest($api_key, $user_email, $latitude, $longtitude, $ip_address, $width_ratio, $ads_count){

		$retVal = false;

		$data= array('api_key' => "$api_key",
			'tag' => 'get_ads',
			'user_email'=>"$user_email",
			'latitude'=>"$latitude",
			'longtitude'=>"$longtitude",
			'ip_address'=>"$ip_address",
			'width_ratio'=>"$width_ratio",
			'ads_count'=>"$ads_count",
			'ads_platform'=>'mobile');

		$url = 'http://dibs.my/api/actionapi.php';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result= curl_exec($ch);
		curl_close($ch);
		$result =  json_decode($result);

		if($result != null) {
			$retVal = $result;
		} else {
			$retVal = false;
		}


		return $retVal;
	}

	public function triggerCpcRequest($api_key, $user_email, $ads_id, $action_id, $latitude, $longtitude){

		$retVal = false;

		$data= array('api_key' => "$api_key",
			'tag' => 'trigger_cpc',
			'user_email'=>"$user_email",
			'latitude'=>"$latitude",
			'longtitude'=>"$longtitude",
			'ads_id'=>"$ads_id",
			'action_id'=>"$action_id",
			'ads_platform'=>'mobile');

		$url = 'http://dibs.my/api/actionapi.php';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result= curl_exec($ch);
		curl_close($ch);
		$result =  json_decode($result);

		if($result != null) {
			$retVal = $result;
		} else {
			$retVal = false;
		}


		return $retVal;
	}

	public function actionRequest($api_key, $user_email, $unique_key, $action_name){
		$retVal = false;
		$data= array('api_key' => "$api_key",
			'tag' => 'trigger_action',
			'user_email'=>"$user_email",
			'unique_key'=>"$unique_key",
			'action_name'=>"$action_name",
			'action_platform'=>'mobile');

		$url = 'http://dibs.my/api/actionapi.php';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result= curl_exec($ch);
		curl_close($ch);
		$result =  json_decode($result);

		if($result != null) {
			$retVal = $result;
		} else {
			$retVal = false;
		}


		return $retVal;
	}

	public function dibsProfileRequest($api_key, $user_email){
		$retVal = false;
		$data= array('api_key' => "$api_key",
			'tag' => 'get_user_profile',
			'user_email'=>"$user_email");

		$url = 'http://dibs.my/api/userapi.php';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result= curl_exec($ch);
		curl_close($ch);
		$result =  json_decode($result);

		if($result != null) {
			$retVal = $result;
		} else {
			$retVal = false;
		}

		return $retVal;
	}

	public function dibsPointRequest($api_key, $user_email){
		$retVal = false;
		$data= array('api_key' => "$api_key",
			'tag' => 'get_user_point',
			'user_email'=>"$user_email");

		$url = 'http://dibs.my/api/userapi.php';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result= curl_exec($ch);
		curl_close($ch);
		$result =  json_decode($result);

		if($result != null) {
			$retVal = $result;
		} else {
			$retVal = false;
		}

		return $retVal;
	}

	public function dibsStoreLoginRequest($email) {
		$encryptionMethod = "AES-256-CBC";  // AES is used by the U.S. gov't to encrypt top secret documents.

		date_default_timezone_set('Asia/Kuala_Lumpur');
		$time = date('Y-m-d H:i:s');

		$iv = uniqid('mhr');

		$encryptedMessage = openssl_encrypt($email . ' ' . $time, $encryptionMethod, $this->hash, false, $iv);
		$encryptedMessage = rawurlencode(rawurlencode($encryptedMessage));

		return $this->dibsLoginUrl . $encryptedMessage . '/KWONG_WAH/' . $iv;
	}


}


?>
