<?php
require_once('../../../wp-config.php');
require_once('../../../wp-includes/wp-db.php');
require_once('../../../wp-includes/pluggable.php');

$current_user = wp_get_current_user();
$cur_userEmail;
$post_userEmail;

// Tag check
if (!isset($_POST['tag']) && $_POST['tag'] == '') {
	echo 'Access Denied';
	exit();
}
$tag = $_POST['tag'];


// Dibs configuration
if (file_exists('/dibsClass.php')) {
	require_once('/dibsClass.php');
} else if (file_exists('dibsClass.php')) {
	require_once('dibsClass.php');
}
$dibs = new dibsClass();


if(isset($_POST['user_email'])) {
	$post_userEmail = $_POST['user_email'];
} else {
	$post_userEmail = "";
}
if(isset($current_user->user_email)) {
	$cur_userEmail = $current_user->user_email;
} else {
	$cur_userEmail = "";
}

// User email check
$userEmail = '';
if ($tag != 'get_ads' && $tag != 'send_cpc') {
	if ($post_userEmail == "" && $cur_userEmail == "") {
		echo json_encode(array('IS_SUCCESS'		=> '0',
							   'response'		=> "Empty User" )
							  );
		return;
	} else if ($cur_userEmail != '') {
		$userEmail = $cur_userEmail;
	} else {
		$userEmail = $post_userEmail;
	}
}


// Get current post data
$curPostData = null;
if(isset($_POST['curPost_Data'])) {
	$curPostData = $_POST['curPost_Data'];
}

$lati = (isset($_POST['lati']) ? $_POST['lati'] : null);
$longi = (isset($_POST['longi']) ? $_POST['longi'] : null);


// Action tag
switch ($tag) {
	case 'vote_mood': // Vote mood
		$prefix_uniqueKey = "kwongwah_post_" . $curPostData["ID"];
		$result = $dibs->voteMood(get_option('dibs_api_key'), $prefix_uniqueKey, $userEmail);

		echo $result;
		break;

	case 'comment': // Comment
		$prefix_uniqueKey = "kwongwah_post_" . $curPostData["ID"];
		$result = $dibs->comment(get_option('dibs_api_key'), $prefix_uniqueKey, $userEmail);

		echo $result;
		break;

	case 'share': // Share
		$posttags = get_the_tags($curPostData["ID"]);
		$reward_process_valid = false;
		$is_reward_tag_set = false;

		if (get_option('share_reward_tag') != '' && get_option('share_reward_tag') != null) {
			$is_reward_tag_set = true;
		}

		if ($is_reward_tag_set) {
			if ($posttags) {
				foreach($posttags as $tag) {
					if (get_option('share_reward_tag') == $tag->name) {
						$reward_process_valid = true;
					}
				}
			}
		} else {
			$reward_process_valid = true;
		}

		if($reward_process_valid) {
			$prefix_uniqueKey = "kwongwah_post_" . $curPostData["ID"];
			$result = $dibs->share(get_option('dibs_api_key'), $prefix_uniqueKey, $userEmail);

			echo $result;
		}

		break;

	case 'get_ads': // Get Ads
	    $ads_count = null;
        if (isset($_POST['ads_count'])) {
            $ads_count = $_POST['ads_count'];
        }

	    echo json_encode($dibs->getAdvertisement(get_option('dibs_api_key'), $ads_count, $cur_userEmail, $lati, $longi));
		break;

	case 'daily_login': // Daily login
		$prefix_uniqueKey = "test_post_" . $curPostData["ID"];
		$result = $dibs->dailyLogin(get_option('dibs_api_key'), $prefix_uniqueKey, $userEmail);

		echo $result;
		break;

	case 'send_cpc': // send cpc to dibs after ads has been triggered
		$ads_id = -1;

		if (isset($_POST['adsId'])) {
			$ads_id = $_POST['adsId'];
		}

		$result = $dibs->sendCpc(get_option('dibs_api_key'), $ads_id, $cur_userEmail, $lati, $longi);

		echo json_encode($result);
		break;
}
?>
