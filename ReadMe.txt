// NOTE //////////////////////////////////////////////////
// This is just a v1.0 prototype for prove of concept & tested working.
// The Dibs advertisement widget is the main feature of this release
// Please feedback to developer if you see any issue in using it
//////////////////////////////////////////////////////////

How to install Dibs System Integration plugin to WP?
====================================================
1) Unzip the file & upload whole folder to your WP /wp-content/plugins folder
2) Goto WP Dashboard->Plugin section to activate "Dibs System Integration"
3) Goto WP Dashboard->Settings->Dibs Settings to fill in your application api_key. For testing purpose, please use Kwong Wah API Key: 69e1fd45cc
4) Goto Appearance->Widget, drag & drop "Dibs Ads Widget" to your preferred location
5) For each widget, select the preferred ads ratio for your location. Currently support "4x3", "16x9". Later will support "3x7"
6) Goto your website press refresh & you should see the ads appear at your target location
