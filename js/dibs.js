var geo_latitude;
var geo_longitude;

jQuery(document).ready(function() {
    // Include Facebook Library
	jQuery.getScript('https://connect.facebook.net/en_US/sdk.js', function(){
		FB.init({
			appId      : app_facebook_id,
			xfbml      : true,
			version    : 'v2.3'
		});
	});

	// Load Dibs Ads    
	var totalAds3 = jQuery(".dibs-ads3").size();
	var totalAds4 = jQuery(".dibs-ads4").size();
	var totalAds16 = jQuery(".dibs-ads16").size();

	// Load to get 3:6 ratio ads
    jQuery.ajax({
        url: dibs_get_engine_url,
        type: "post",
        data: {
                tag: "get_ads",
                ads_count: totalAds3,
              },
        success: function(result) {
            jQuery(".dibs-ads3").each(function(index, element) {
                var result_obj = jQuery.parseJSON(result);
			
				if(result_obj == null){
			        return;
			    }

                var ads_type = jQuery(element).find("input[name=dibs-ads-type]").val();
                if (ads_type == "3") {
                    ads_type = "image_3_6";
                }
			        
                jQuery(element).find(".dibs-ads-container").html(
                    "<label>赞助广告</label><a class='hoverHand' onclick=sendCPC('"+result_obj[index]['target_link']+"',"+result_obj[index]['ads_id']+");><img src='"+result_obj[index][ads_type]+"' style='width:100%;height:auto;'></a>"
                );
            });
        },
        error:function(err) {
            console.log(err);
        }   
    });
	// Load to get 4:3 ratio ads
	jQuery.ajax({
        url: dibs_get_engine_url,
        type: "post",
        data: {
                tag: "get_ads",
                ads_count: totalAds4,
              },
        success: function(result) {
            jQuery(".dibs-ads4").each(function(index, element) {
                var result_obj = jQuery.parseJSON(result);
			
				if(result_obj == null){
			        return;
			    }

                var ads_type = jQuery(element).find("input[name=dibs-ads-type]").val();
                if (ads_type == "4") {
                    ads_type = "image_4_3";
                }
			        
                jQuery(element).find(".dibs-ads-container").html(
                    "<label>赞助广告</label><a class='hoverHand' onclick=sendCPC('"+result_obj[index]['target_link']+"',"+result_obj[index]['ads_id']+");><img src='"+result_obj[index][ads_type]+"' style='width:100%;height:auto;'></a>"
                );
            });
        },
        error:function(err) {
            console.log(err);
        }   
    });
	// Load to get 16:9 ratio ads
	jQuery.ajax({
        url: dibs_get_engine_url,
        type: "post",
        data: {
                tag: "get_ads",
                ads_count: totalAds16,
              },
        success: function(result) {
            jQuery(".dibs-ads16").each(function(index, element) {
                var result_obj = jQuery.parseJSON(result);
			
				if(result_obj == null){
			        return;
			    }

                var ads_type = jQuery(element).find("input[name=dibs-ads-type]").val();
                if (ads_type == "16") {
                    ads_type = "image_16_9";
                }
			        
                jQuery(element).find(".dibs-ads-container").html(
                    "<label>赞助广告</label><a class='hoverHand' onclick=sendCPC('"+result_obj[index]['target_link']+"',"+result_obj[index]['ads_id']+");><img src='"+result_obj[index][ads_type]+"' style='width:100%;height:auto;'></a>"
                );
            });
        },
        error:function(err) {
            console.log(err);
        }   
    });


	// Get location Track
	var allowTrack = jQuery.cookie("locationTrack");
	if(allowTrack == null || allowTrack == "" || allowTrack == "true") {
		jQuery.cookie("locationTrack", false, {
                           expires : 1,
                           path    : "/",
                           secure  : false
                        });

		getLocation(function() {

			jQuery.cookie("locationTrack", true, {
                           expires : 1,
                           path    : "/",
                           secure  : false
                        });
		});
 	}

	// Load Background Ads
	if(jQuery(".dibs-background-ads").length > 0 ) {
		jQuery.ajax({
        	url: dibs_get_engine_url,
        	type: "post",
        	data:{
                	tag: "get_ads",
                	ads_count: 1,
                	lati: geo_latitude,
					longi: geo_longitude
            	 },
        	success: function(result) {
	            var ads_type = "image_16_9";
	            var result_obj = jQuery.parseJSON(result);
			
				if(result_obj == null){
			        return;
			    }

				jQuery(document).find("#main-content").css('background-image', 'url('+result_obj[0][ads_type]+')');
				jQuery(document).find("#main-content").css('background-repeat', 'no-repeat');
				jQuery(document).find("#main-content").css('background-size', 'contain');
				jQuery(document).find("#main-content").css('background-position', 'center');
        	},
        	error:function(err) {
            	console.log(err);
        	}
        });
	}

});

// Include Twitter Library
window.twttr = (function (d,s,id) {
	var t, js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {
		return;
	}
	js=d.createElement(s);
	js.id=id;
	js.src="https://platform.twitter.com/widgets.js";
	fjs.parentNode.insertBefore(js, fjs);

	return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f) } });
}(document, "script", "twitter-wjs"));



// Register callback from twitter for DIBS Reward
twttr.ready(function (twttr) {
	var sh_curPostData	= jQuery.parseJSON(dibs_get_current_post);

    twttr.events.bind('tweet', function (event) {
        jQuery.ajax({
			url: dibs_get_engine_url,
			type: "post",
			data: {tag: 'share',
				   curPost_Data: sh_curPostData
				  },
			success: function(data) {
				var decoded_data = jQuery.parseJSON(data);
			
				if(decoded_data == null){
			        return;
			    }
			        
				if (decoded_data.IS_SUCCESS > 0) {
					dibs_dialog_open(decoded_data.messageType, decoded_data.message);
				}
			},
			error:function(err) {
				console.log(err);
			}   
		});
		return;
    });
});


// Share to Facebook & callback for DIBS Reward
function shareOnFacebook() {
	var sh_data 		= jQuery.parseJSON(dibs_get_share_data);
	var sh_curPostData	= jQuery.parseJSON(dibs_get_current_post);

	FB.ui({
		method: 'share',
		href: sh_data["url"]
	}, function(response) {
		if (response && !response.error_code) {
	      	jQuery.ajax({
				url: dibs_get_engine_url,
				type: "post",
				data: {tag: 'share',
					   curPost_Data: sh_curPostData
					  },
				success: function(data) {
					var decoded_data = jQuery.parseJSON(data);
			
					if(decoded_data == null){
				        return;
				    }
				        
				  	if (decoded_data.IS_SUCCESS > 0) {
						dibs_dialog_open(decoded_data.messageType, decoded_data.message);
				  	} else {
				  		console.log(decoded_data.response);
				  	}
				},
				error:function(err) {
					console.log(err);
				}   
			});
			return; 
	    } else {
	     	console.log('Post was not shared to facebook');
	     	return;
	    }
	});
}


// Comment to get DIBS Reward
function commentsReward(force_reload) {
	var sh_curPostData	= jQuery.parseJSON(dibs_get_current_post);
	jQuery.ajax({
		url: dibs_get_engine_url,
		type: "post",
		data: {tag: 'comment',
			   curPost_Data: sh_curPostData
			  },
		success: function(data) {
			var decoded_data = jQuery.parseJSON(data);
			
			if(decoded_data == null){
		        return;
		    }
		        
			if (decoded_data.IS_SUCCESS > 0) {
				dibs_dialog_open(decoded_data.messageType, decoded_data.message, force_reload);
			}
		},
		error:function(err) {
			console.log(err);
			if(force_reload) {
				location.reload();
			}
		}
	});
}


// mood vote to get DIBS Reward
function moodVoteReward() {
	var sh_curPostData	= jQuery.parseJSON(dibs_get_current_post);

	jQuery.ajax({
		url: dibs_get_engine_url,
		type: "post",
		data: {tag: 'vote_mood',
			   curPost_Data: sh_curPostData
			  },
		success: function(data) {
			var decoded_data = jQuery.parseJSON(data);
			
			if(decoded_data == null){
		        return;
		    }
		        
			if (decoded_data.IS_SUCCESS > 0) {
				dibs_dialog_open(decoded_data.messageType, decoded_data.message);
			}
		},
		error:function(err) {
			console.log(err);
		}
	});
	return;
}


// Get Dibs Account activation
function requestDibsActivation(request_data) {
	jQuery.ajax({
		url: dibs_get_engine_url,
		type: "post",
		data: {tag: 'register',
			   email: request_data["email"],
			   userName: request_data["user_name"]},
		success: function(data) {
			console.log(data);
		},
		error:function(err) {
			console.log(err);
		}   
	});
	return;
}


// Daily login to get DIBS Reward
function dailyLoginReward() {
	jQuery.ajax({
		url: dibs_get_engine_url,
		type: "post",
		data: {tag: 'daily_login'
			  },
		success: function(data) {
			var decoded_data = jQuery.parseJSON(data);

			if(decoded_data == null){
		        return;
		    }
		        
			if (decoded_data.IS_SUCCESS > 0) {
				dibs_dialog_open(decoded_data.messageType, decoded_data.message);
			}
		},
		error:function(err) {
			console.log(err);
		}
	});
	return;
}


// Show Advertisement after sometimes
function popAdsAfter(delay_time) {
	setTimeout(function() {
		jQuery.ajax({
	        url: dibs_get_engine_url,
	        type: "post",
	        data:{
	                tag: "get_ads",
	                ads_count: 1,
	                lati: geo_latitude,
					longi: geo_longitude
	            },
	        success: function(result) {
	            var result_obj = jQuery.parseJSON(result);
	            
	            if(result_obj == null){
		        	return;
		        }

	            var ads_type = "image_4_3";
	            var message_type = "Advertisement";

		        var message = {
		        				"ads" : "<a class='hoverHand' onclick=sendCPC('"+result_obj[0]['target_link']+"',"+result_obj[0]['ads_id']+");><img src='"+result_obj[0][ads_type]+"' style='width:100%;height:auto;'></a>",
		        				"ads_url" : result_obj[0]['target_link'],
		        				"ads_id" : result_obj[0]['ads_id']
		        			  };
		        
	            if (result_obj.IS_SUCCESS > 0) {
	                dibs_dialog_open(message_type, "", false, message);
	            }

	        },
	        error:function(err) {
	            console.log(err);
	        }   
	    });
	}, delay_time);
}


// Trigger CPC from ads
function sendCPC(ads_url, ads_id) {

	jQuery.ajax({
		url: dibs_get_engine_url,
		type: "post",
		data: {	
				tag: 'send_cpc',
				adsId: ads_id,
				lati: geo_latitude,
				longi: geo_longitude
			  },
		success: function(result) {
			var decoded_data = jQuery.parseJSON(result);

			if(decoded_data == null) {
			    return;
			}
			        
			if (decoded_data.IS_SUCCESS > 0) {
				var win = window.open(ads_url);
	  			win.focus();
			}
		},
		error:function(err) {
			console.log(err);
		}
	});

}


// Get geolocation
function getLocation(callback) {
	var setPosition;
	
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition( function(position) {

	        geo_latitude = position.coords.latitude;
	        geo_longitude = position.coords.longitude;

	        callback();
	    }, function(error) {
	        geolocFail();
	        callback();
	    });

    } else {
        // x.innerHTML = "Geolocation is not supported by this browser.";
        callback();
    }
}
