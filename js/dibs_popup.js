jQuery(document).ready(function() {
	jQuery("body").append('<div class="dibs-overlay"></div>' +
						  '<div class="dibs-dialog" id="dibs-dialog">' +
							'<div class="dibs-dialog-box">' +
								'<div class="dibs-dialog-header">' +
									'<h3></h3>' +
								'</div>' +
								'<div class="dibs-dialog-body">' +
									'<div class="dibs-dialog-content"></div>' +
									'<div class="dibs-dialog-ads"></div>' +
								'</div>' +
								'<div class="dibs-dialog-footer">' +
									'<input type="button" class="visit-btn" value="Visit Link" />' +
									'<input type="button" class="close-btn dibs-btn-close" value="Close" />' +
								'</div>' +
							'</div>' +
						  '</div>');
	

			var dibs_dialog 		= jQuery('#dibs-dialog');
			var dibs_overlay 		= jQuery('.dibs-overlay');
			var dibs_close 			= dibs_dialog.find('.close-btn');
			var dibs_box 			= dibs_dialog.find('.dibs-dialog-box');
			var dibs_dialog_title	= jQuery('.dibs-dialog-header h3');
			var dibs_dialog_footer	= jQuery('.dibs-dialog-footer');
			var dibs_dialog_content	= jQuery('.dibs-dialog-content');
			var dibs_dialog_ads		= jQuery('.dibs-dialog-ads');

			var device_detect = false;
			if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
				device_detect = true;
			}

			var forceReload 	= false;

			var ads_link;
			var ads_id;

		window.dibs_dialog_open = function (p_title, p_content, p_forceReload, p_advertisement) {
			forceReload = p_forceReload;


			// Check whether any dialog showing
			if(!dibs_dialog.hasClass('in')) {
				var msg_content;

				if(p_content != null && p_content != "") {
					msg_content = '<div class="dibs_award_icon"><img src="' + p_content["award_icon"] + '" /></div>' +
								  '<div class="dibs_reward_msg">' +
										'<h4 class="dibs_reward_dialog_title">' + p_content["action"] + '</h4>' +
										'<p class="dibs_reward_dialog_desc">' +
											p_content["action_desc"] +
										'</p>' +
								  '</div>' +
								  '<div class="dibs_reward_dialog_items">' +
								  		'<div class="items_point">' +
								  			'<img src="' + dibs_get_plugin_url + '/dibs_engine/icon/ads_points.png" />' +
								  			'<text>' + p_content["point"] + '</text>' +
								  		'</div>' +
								  		'<div class="items_coin">' +
								  			'<img src="' + dibs_get_plugin_url + '/dibs_engine/icon/ads_coin.png" />' +
								  			'<text>' + p_content["coin"] + '</text>' +
								  		'</div>' +
								  '</div>';

					jQuery.ajax({
				        url: dibs_get_engine_url,
				        type: "post",
				        data:{
				                tag: "get_ads",
				                ads_count: 1,
	                			lati: geo_latitude,
								longi: geo_longitude 
				            },
				        success: function(result) {
				            var result_obj = jQuery.parseJSON(result);
				            
				            if(result_obj == null){
					        	return;
					        }

				            var ads_type = "image_4_3";
					        var message = "<a class='hoverHand' onclick=sendCPC('"+result_obj[0]['target_link']+"',"+result_obj[0]['ads_id']+");><img src='"+result_obj[0][ads_type]+"' style='width:100%;height:auto;'></a>";
					        
					        ads_link = result_obj[0]['target_link'];
							ads_id = result_obj[0]['ads_id'];
					        
				            if (result_obj.IS_SUCCESS > 0) {
				            	dibs_dialog_ads.html(message);
				            	
				            	if(dibs_dialog_ads.html()) {
									jQuery(".visit-btn").css('display', 'inline-block');
								} else {
									jQuery(".visit-btn").css('display', 'none');
								}
				            }

				        },
				        error:function(err) {
				            console.log(err);
				        }
				    });
				} else {
					msg_content = "";
				}
				

				//Set in content and title
				if(p_title)
				dibs_dialog_title.text(p_title);
				dibs_dialog_content.html(msg_content);

				if(p_advertisement != null && p_advertisement != ""){
					ads_link = p_advertisement["ads_url"];
					ads_id = p_advertisement["ads_id"];

					dibs_dialog_ads.html(p_advertisement["ads"]);
					dibs_dialog_title.css('display', 'none');
					dibs_dialog_content.css('display', 'none');
				} else {
					dibs_dialog_title.css('display', 'block');
					dibs_dialog_content.css('display', 'block');
				}

				if(dibs_dialog_ads.html()) {
					jQuery(".visit-btn").css('display', 'inline-block');
				} else {
					jQuery(".visit-btn").css('display', 'none');
				}

				//Get the scrollbar width and avoid content being pushed
				var w1 = jQuery(window).width();
				jQuery('html').addClass('dibs-dialog-open');
				var w2 = jQuery(window).width();
				c = w2-w1 + parseFloat(jQuery('body').css('padding-right'));
				if( c > 0 ) jQuery('body').css('padding-right', c + 'px' );

				if(!device_detect) {
					jQuery('body > div:not(.dibs-dialog)').addClass('blurblur');
				}

				dibs_overlay.fadeIn('fast');
				dibs_dialog.show( 'fast', function(){
					dibs_dialog.addClass('in');
				});
			}
		}	

		
		// Visit link click open new tab
		dibs_dialog.find(".visit-btn").click(function(e){
			sendCPC(ads_link, ads_id);
			// window.open(dibs_dialog_ads.find("a").attr("href"));
		});


		// Close dialog when clicking on the close button
		dibs_close.click(function(e){			
			dibs_dialog.removeClass('in').delay(150).queue(function(){
				dibs_dialog.hide().dequeue();	
				dibs_overlay.fadeOut('slow');
				jQuery('html').removeClass('dibs-dialog-open');
				jQuery('body').css('padding-right', '');
				jQuery('body > div').removeClass('blurblur');		
			});

			if(forceReload) {
				location.reload();
			}
			return false;
		});

		// Close dialog when clicking outside the dialog
		dibs_dialog.click(function(e){
			dibs_close.trigger('click');		
		});
		dibs_box.click(function(e){
			e.stopPropagation();
		});	
});


		