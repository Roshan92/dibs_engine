<?php
class DibsClass {
	public $dibsUrl = array(
		'action' 	=> 'http://dibs.my/api/actionapi.php',
		'user' 		=> 'http://dibs.my/api/userapi.php'
	);

	// Send request to dibs api with curl
	public function http_post($url, $post) {
		$c = curl_init();
		curl_setopt($c, CURLOPT_URL, $url);

		curl_setopt($c, CURLOPT_POST, true);
		curl_setopt($c, CURLOPT_POSTFIELDS, $post);

		curl_setopt ($c, CURLOPT_RETURNTRANSFER, true);
		return curl_exec($c);
	}

	// Set dibs toast message for user
	public function setDibsAjaxMessage($action, $coin, $point, $reward_icon) {
		$award_description = '恭喜! 您获得 “'.$action.'“ 的奖励!';
		$rewardMessage = array( 'action' 		=> $action,
								'coin'			=> $coin,
								'point'		 	=> $point,
								'action_desc'	=> $award_description,
								'award_icon'	=> $reward_icon
								);

		return $rewardMessage;
	}

	// Register new user to connect with dibs
	public function register($apiKey, $email, $userName) {
		$postdata = http_build_query(array(
			'api_key' 		=> $apiKey,
			'tag' 			=> 'register',
			'user_email' 	=> $email,
			'user_name' 	=> $userName
		));
		return $this->http_post($this->dibsUrl['user'], $postdata);
	}

	// Submit mood action reward
	public function voteMood($apiKey, $uniqueKey, $email) {
		$postdata = http_build_query(array(
			'api_key' 		=> $apiKey,
			'tag' 			=> 'trigger_action',
			'user_email' 	=> $email,
			'action_name' 	=> 'vote_mood',
			'unique_key' 	=> $uniqueKey
		));
		$action = json_decode($this->http_post($this->dibsUrl['action'], $postdata));

		if ($action->IS_SUCCESS == '1') {
			$message = $this->setDibsAjaxMessage($action->action_desc, $action->reward_coin, $action->reward_point, $action->icon_image);

			return json_encode(array('IS_SUCCESS' 	=> '1',
									 'messageType'	=> 'Reward',
									 'message'		=> $message )
							   );
		} else {

			return json_encode(array('IS_SUCCESS'	=> '0',
									 'response'		=> $action->RESPOND )
							  );
		}
	}

	// Submit comment action reward
	public function comment($apiKey, $uniqueKey, $email) {
		$postdata = http_build_query(array(
			'api_key' 		=> $apiKey,
			'tag' 			=> 'trigger_action',
			'user_email' 	=> $email,
			'action_name' 	=> 'comment',
			'unique_key' 	=> $uniqueKey
		));
		$action = json_decode($this->http_post($this->dibsUrl['action'], $postdata));

		if ($action->IS_SUCCESS == '1') {
			$message = $this->setDibsAjaxMessage($action->action_desc, $action->reward_coin, $action->reward_point, $action->icon_image);

			return json_encode(array('IS_SUCCESS' 	=> '1',
									 'messageType'	=> 'Reward',
									 'message'		=> $message )
							   );
		} else {

			return json_encode(array('IS_SUCCESS'	=> '0',
									 'response'		=> $action->RESPOND )
							  );
		}
	}

	// Submit share action reward
	public function share($apiKey, $uniqueKey, $email) {
		$postdata = http_build_query(array(
			'api_key' 		=> $apiKey,
			'tag' 			=> 'trigger_action',
			'user_email' 	=> $email,
			'action_name' 	=> 'share',
			'unique_key' 	=> $uniqueKey
		));
		$action = json_decode($this->http_post($this->dibsUrl['action'], $postdata));

		if ($action->IS_SUCCESS == '1') {
			$message = $this->setDibsAjaxMessage($action->action_desc, $action->reward_coin, $action->reward_point, $action->icon_image);

			return json_encode(array('IS_SUCCESS' 	=> '1',
									 'messageType'	=> 'Reward',
									 'message'		=> $message )
							   );
		} else {

			return json_encode(array('IS_SUCCESS'	=> '0',
									 'response'		=> $action->RESPOND )
							  );
		}
	}

	// Submit daily login action reward
	public function dailyLogin($apiKey, $uniqueKey, $email) {
		$postdata = http_build_query(array(
			'api_key' 		=> $apiKey,
			'tag' 			=> 'trigger_action',
			'user_email' 	=> $email,
			'action_name' 	=> 'daily_login',
			'unique_key' 	=> $uniqueKey
		));
		$action = json_decode($this->http_post($this->dibsUrl['action'], $postdata));

		if ($action->IS_SUCCESS == '1') {
			$message = $this->setDibsAjaxMessage($action->action_desc, $action->reward_coin, $action->reward_point, $action->icon_image);

			return json_encode(array('IS_SUCCESS' 	=> '1',
									 'messageType'	=> 'Reward',
									 'message'		=> $message )
							   );
		} else {

			return json_encode(array('IS_SUCCESS'	=> '0',
									 'response'		=> $action->RESPOND )
							  );
		}
	}

	// Submit for new register reward
	public function newRegisterReward($apiKey, $uniqueKey, $email) {
		$postdata = http_build_query(array(
			'api_key'		=> $apiKey,
			'tag'			=> 'trigger_action',
			'user_email'	=> $email,
			'action_name'	=> 'register',
			'unique_key'	=> $uniqueKey
		));
		return $this->http_post($this->dibsUrl['action'], $postdata);
	}

	// Request for large size advertisment
	public function getAdvertisement($apiKey, $ads_count, $email, $latitude, $longitude) {

		$postdata = http_build_query(array(
			'api_key' 		=> $apiKey,
			'tag' 			=> 'get_ads',
			'width_ratio'	=> 4,
			'user_email'	=> $email,
			'ads_count'		=> $ads_count,
			'ip_address'	=> $this->get_user_ip(),
			'ads_platform'	=> 'web',
			'latitude'		=> $latitude,
			'longitude'		=> $longitude
		));

		$action = json_decode($this->http_post($this->dibsUrl['action'], $postdata), true);

		return $action;
	}

	// Send CPC to dibs
	public function sendCpc($apiKey, $ads_id, $email, $latitude, $longitude) {

		$postdata = http_build_query(array(
			'api_key' 		=> $apiKey,
			'tag' 			=> 'trigger_cpc',
			'ads_id'		=> $ads_id,
			'user_email'	=> $email,
			'ip_address'	=> $this->get_user_ip(),
			'ads_platform'	=> 'web',
			'latitude'		=> $latitude,
			'longitude'		=> $longitude
		));

		$action = json_decode($this->http_post($this->dibsUrl['action'], $postdata), true);

		return $action;
	}

	// Get user ip address
	public function get_user_ip() {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			//check ip from share internet
			$ip = $_SERVER['HTTP_CLITENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			//to check ip is pass from proxy
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		return $ip;
	}

}
?>
