<?php
/*
Plugin Name: DIBS System Integration
Plugin URI: http://www.dibs.my/
Description: This is a custom plugin for integrating dibs into wordpress web page
Version: 1.0
Author: Guess Who Am I
Author URI:
License: DIBS
*/

// Dibs configuration
require_once('dibsClass.php');
require_once('dibs_widget.php');

// Register Dibs Widget
add_action( 'widgets_init', function(){
     register_widget( 'Dibs_Widget' );
});

// Register Dibs Setting page
function dibs_setting_menu() {
	add_options_page( 'Dibs Setting', 'Dibs Setting', 'manage_options', 'dibs-setting', 'get_dibs_setting' );

	// Register Dibs settings
	register_setting( 'dibs-settings', 'dibs_api_key' );
    register_setting( 'dibs-settings', 'app_facebook_id' );
    register_setting( 'dibs-settings', 'share_reward_tag' );
}
add_action( 'admin_menu', 'dibs_setting_menu' );

// Initialise Dibs Setting page
function get_dibs_setting() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

?>
	<div class="wrap">
	    <h2>Dibs Setting</h2>
	    <form method="post" action="options.php">
	        <?php settings_fields( 'dibs-settings' ); ?>
	        <?php do_settings_sections( 'dibs-settings' ); ?>
            <table class="form-table">
                <tr valign="top">
                <th scope="row">API Key (Unique to this App)</th>
                <td><input type="text" name="dibs_api_key" value="<?php echo esc_attr( get_option('dibs_api_key') ); ?>" /></td>
                </tr>
                <tr valign="top">
                <th scope="row">Facebook app id</th>
                <td><input type="text" name="app_facebook_id" value="<?php echo esc_attr( get_option('app_facebook_id') ); ?>" /></td>
                </tr>
                <tr valign="top">
                <th scope="row">Share reward Tag <br> (Leave it blank for all article or tag for specific article to get reward.)</th>
                <td><input type="text" name="share_reward_tag" value="<?php echo esc_attr( get_option('share_reward_tag') ); ?>" /></td>
                </tr>
                <tr valign="top">
                <th scope="row">Shortcode Dibs ads</th>
                <td style="border: 1px solid">
                    <p>Example: [dibs_setAds_widget ratio="16" align="left" size="half" qty="single"]</p>
                    <p>Ratio: 16, 4, 3</p>
                    <p>Size: half, full</p>
                    <p>Align: left or right</p>
                    <p>Qty: single or multiple</p>
                </td>
                </tr>
            </table>
            <?php submit_button(); ?>
        </form>
	</div>
<?php
}

// Show error alert if user enabled Dibs plugin but without API key filled in
function show_alert_no_api_key(){
    global $pagenow;
    if ( !get_option('dibs_api_key') && $pagenow=='plugins.php' ) {
         echo '<div class="error">
             <p>Please goto Settings->Dibs Setting to update API Key for your DIBS System to work properly.</p>
         </div>';
    }
}
add_action('admin_notices', 'show_alert_no_api_key');

function dibs_add_enqueue_to_footer() {
	wp_localize_script('jquery', 'dibs_get_engine_url', dibs_get_engine_script());
    wp_localize_script('jquery', 'dibs_get_plugin_url', dibs_get_plugin_url());
    wp_localize_script('jquery', 'app_facebook_id', app_facebook_id());
    wp_localize_script('jquery', 'dibs_get_current_post', dibs_get_post_script());
    wp_localize_script('jquery', 'dibs_get_share_data', dibs_get_share_data());
	  wp_enqueue_script('dibs-js', plugins_url('js/dibs.js', __FILE__), array('jquery'), null, true);
    wp_enqueue_script('dibs_popup', plugins_url('js/dibs_popup.js', __FILE__), array('jquery'), null, true);
    wp_enqueue_style('dibs_style', plugins_url('css/style.css', __FILE__));
    wp_enqueue_script('jquery.cookie', plugins_url('js/jquery.cookie.js', __FILE__), array('jquery'), null, true);
}
add_action('wp_enqueue_scripts', 'dibs_add_enqueue_to_footer');

// Return plugin engine url
function dibs_get_engine_script() {
    $engine_url = plugins_url(). '/dibs_engine/dibs.php';

    return $engine_url;
}
// Return plugin url
function dibs_get_plugin_url() {
    $url = plugins_url();

    return $url;
}
function app_facebook_id() {
    $fb_id = get_option('app_facebook_id');

    return $fb_id;
}
// Return current post data
function dibs_get_post_script() {
    $post_object = json_encode(get_post());

    return $post_object;
}
// Return JSON array of post sharing data
function dibs_get_share_data() {
    global $post;
    $share_array = array("url"      => get_permalink(),
                        //  "desc"     => get_excerpt(140),
                         "title"    => get_the_title(),
                         "thumb"    => wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium')[0],
                         "dibs_msg" => "Share to get rewarded from DIBS"
                        );

    return json_encode($share_array);
}


// Fetch advertisement from DIBS with shortcode
function dibs_getAdvertisement($atts) {
	extract(shortcode_atts(array(
      'ratio' => 16,
      'align' => 'left',
      'size' => 'half',
      'qty' => 'single'
    ), $atts));

    if($size == 'half') {
        $size = "insert-post-ads-halfSize";
    } else {
        $size = "insert-post-ads-fullSize";
    }

    if($align == 'left') {
        $align = "insert-post-ads-pullLeft";
    } else {
        $align = "insert-post-ads-pullRight";
    }

    if($qty == 'single') {
        $qty = "insert-post-ads-single";
    } else {
        $qty = "insert-post-ads-multiple";
    }

    if($ratio == 4) {
        $container = '<div class="dibs-ads4"><input type="hidden" name="dibs-ads-type" value="'.$ratio.'"><div class="dibs-ads-container '.$align.' '.$size.' '.$qty.'"></div></div>';
    } else if($ratio == 3) {
        $container = '<div class="dibs-ads3"><input type="hidden" name="dibs-ads-type" value="'.$ratio.'"><div class="dibs-ads-container '.$align.' '.$size.' '.$qty.'"></div></div>';
    } else {
        $container = '<div class="dibs-ads16"><input type="hidden" name="dibs-ads-type" value="'.$ratio.'"><div class="dibs-ads-container '.$align.' '.$size.' '.$qty.'"></div></div>';
    }

	return $container;
}
add_shortcode('dibs_setAds_widget', 'dibs_getAdvertisement');


// Fetch advertisement to background
function dibs_setBackgroundAdvertisement() {
    $contentDiv = '<div class="dibs-background-ads"></div>';
    echo $contentDiv;
}
add_action('dibs_setAds_background', 'dibs_setBackgroundAdvertisement');


// User confirmation activation request to DIBS
function dibs_register_user($user_id) {
    $user = new WP_User( $user_id );
    $dibs = new dibsClass();

    $dibs->register(get_option('dibs_api_key'), $user->user_email, $user->user_login);

    return;
}
add_action( 'user_register', 'dibs_register_user' );

// add_action( 'dibs_register', 'dibs_register_user' );


// Reward newly register user
function register_user_reward($user_id) {
    $user = new WP_User( $user_id );
    $dibs = new dibsClass();

    $uniqueKey = "register_" . $user->user_email;

    $dibs->newRegisterReward(get_option('dibs_api_key'), $uniqueKey, $user->user_email);

    return;
}
add_action( 'user_register', 'register_user_reward' );


// DIBS FB Share
function dibs_share_fb_link() {
    $shareFbBtn = 'onclick="shareOnFacebook()"';

    return $shareFbBtn;
}
add_shortcode('dibs_shareFb_link', 'dibs_share_fb_link');

// DIBS TW Tweet
function dibs_share_tw_link() {
    $tweetHashTag = "Tweet_to_get_rewarded_from_DIBS";

    $tweetBtn = 'href="https://twitter.com/intent/tweet?url=' . urlencode(get_permalink()) . '&text=' . get_the_title() . '&hashtags=' . $tweetHashTag . '"';

    return $tweetBtn;
}
add_shortcode('dibs_tweet_link', 'dibs_share_tw_link');


// Daily login for Reward
function dibs_daily_login() {
    if (is_user_logged_in()) {
        if (!isset($_COOKIE['dailyReward'])) {
            echo '<script type="text/javascript">
                    jQuery(window).load(function() {
                        dailyLoginReward();

                        jQuery.cookie("dailyReward", true, {
                           expires : 1,
                           path    : "/",
                           secure  : false
                        });
                    });
                  </script>';
        }
    } else {
        if (isset($_COOKIE['dailyReward'])) {
            echo '<script type="text/javascript">
                    jQuery(window).load(function() {
                       jQuery.removeCookie("dailyReward", { path : "/" });
                    });
                  </script>';
        }
    }
}
add_action('wp_head', 'dibs_daily_login');


// Pop advertisement in front page after sometimes
function dibs_pop_ads() {
    $delayTime = 1000 * 3; // 3 seconds

    $to_trigger = true; // Make trigger 100%

    // 20% to trigger popup ads
    // for($i = 0; $i < 2; $i++) {
    //     $random_num[$i] = rand(0,10);
    // }
    // $to_pair = rand(0,10);
    // $to_trigger = false;


    // $items_count = sizeof($random_num);
    // for($i = 0; $i < $items_count; $i++) {
    //     if($to_pair == $random_num[$i]) {
    //         $to_trigger = true;
    //     }
    // }

    if (is_front_page() && $to_trigger) {
        echo '<script type="text/javascript">
                jQuery(window).load(function() {
                    popAdsAfter('.$delayTime.');
                });
              </script>';
    }
}
add_action('wp_footer', 'dibs_pop_ads');


// Social Share button
function dibs_social_share() {
    $twitterBtn = do_shortcode("[dibs_tweet_link]");
    $button = ' <div id="share-buttons">'.
                    '<p>分享</p>'.
                    '<ul>'.
                        '<li><a onclick="shareOnFacebook()"><i class="fa fa-facebook"></i></a></li>'.
                        '<li><a '. $twitterBtn . '><i class="fa fa-twitter"></i></a></li>'.
                    '</ul>'.
                '</div>';

    if(is_singular() && !is_front_page()) {
        echo "<script type='text/javascript'>
                jQuery(document).ready(function() {
                    jQuery('body').append('". $button ."');
                });
              </script>";
    }
}
add_action('wp_head', 'dibs_social_share');

?>
